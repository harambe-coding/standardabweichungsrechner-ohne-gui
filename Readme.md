#Derzeit nur ohne GUI verfügbar!


##Standardabweichungsrechner

Ein in Java geschriebener Rechner für Durchschnitte und Standardabweichungen.


##Voraussetzungen
Um den Rechner nutzen zu können benötigen Sie Oracle(R) Java(TM) Version 8.
Erhältlich unter: [Java.com](http://java.com)

##Download
Das Programm steht als JAR (.jar) Datei zur Verfügung und kann über diesen Link heruntergeladen werden:
[Download](https://bitbucket.org/harambe-coding/standardabweichungsrechner-ohne-gui/downloads/Standardabweichungsrechner-ohne-GUI.jar)

##Programm starten

###Windows

CMD öffnen: `Windows + R` drücken und `cmd` in das Feld eintragen. Bestätigen mit  `ENTER`.


In das Verzeichnis navigieren, in dem sich die JAR (.jar) befindet.


Dann folgendes eingeben: `java -jar Standardabweichungsrechner-ohne-GUI.jar`

###Linux und OS X
Terminal öffnen und in das Verzeichnis navigieren, indem sich die JAR (.jar) befindet.


Dann folgendes eingeben: `java -jar Standardabweichungsrechner-ohne-GUI.jar`

##Eingeben von Messwerten
Kommazahlen bitte mit ` , `(Komma) trennen und nach jedem eingegebenem Messwert `ENTER` drücken.