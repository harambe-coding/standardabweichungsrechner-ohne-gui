package harambecoding;

import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.RED;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import org.fusesource.jansi.AnsiConsole;

	public class Physik {
	  public static void main(String[] args) throws IOException {
		AnsiConsole.systemInstall();
		
	    Scanner scan = new Scanner(System.in);        
	    FileWriter fw = new FileWriter("ausgabe.txt");  
	    BufferedWriter bw = new BufferedWriter(fw);     
	    
	    //Abfrage der Werte + erstellung der Arrays
	    clear();
	    System.out.println(ansi().fg(RED).a("___________________________________________________________________________________________").reset());
	    System.out.println("      @@:		 __   __  _______  ______    _______  __   __  _______  _______ ");
	    System.out.println("     @@@@@		|  | |  ||   _   ||    _ |  |   _   ||  |_|  ||  _    ||       |");
	    System.out.println("    +@@@@@@		|  |_|  ||  |_|  ||   | ||  |  |_|  ||       || |_|   ||    ___|");
	    System.out.println("    @@@@@@@@.		|       ||       ||   |_||_ |       ||       ||       ||   |_");
	    System.out.println("   #@@@@@@@@@`		|       ||       ||    __  ||       ||       ||  _   | |    ___|");
	    System.out.println("   @@@@@@@@@@@		|   _   ||   _   ||   |  | ||   _   || ||_|| || |_|   ||   |___ ");
	    System.out.println("  :@@@@@@@@@@@;		|__| |__||__|_|__||___|__|_||__|_|__||_|_  |_||_______||_______|");
	    System.out.println("  @@@@@@@@@@@@        		c	o	d	i	n	g");
	    System.out.println(" `@@@@@@@@@@@@	");
	    System.out.println(" @@@@@@@@@@@@@@;");
	    System.out.println(",@@@@@@@@@@@@@@@");
	    System.out.println("   ,@@@@@@@@@@@@@");
	    System.out.println("      .#@@@@@@@@#");
	    System.out.println("         .#@@@@@");
	    System.out.println("___________________________________________________________________________________________");
	    System.out.println();
	    System.out.println();
	    System.out.println("Standardabweichungsrechner von Felix Vorwerk und Kevin Cilek");
	    System.out.println("Version 0.5 Alpha");
	    System.out.println("https://bitbucket.org/harambe-coding/");
	    System.out.println();
	    System.out.println(ansi().fg(RED).a("Wie viele Werte wollen sie eingeben?").reset());
	    int anzahl = scan.nextInt();
	    int anzahlII = anzahl;
	    int arrayBefuellen = anzahl - 1;
	    double[] werte = new double[anzahl];

	    //Werte einlesen
	    clear();
	    System.out.println("Bitte Werte eingeben");
	    int stelle = 1;
	    for (int i = 0 ; i<=arrayBefuellen ; i++ ) {
	      clear();
	      System.out.println(stelle+". Wert");
	      Scanner scan1 = new Scanner(System.in);
	      werte[i] = scan1.nextDouble();
	      System.out.println(werte[i]);
	      stelle++;
	      clear();
	    }
	    
	    
	    //Ausgabe der Werte
	    System.out.println("Die eigegebenen Werte sind: " + Arrays.toString(werte));
	    System.out.println(); 
	    System.out.println();
	    
	    //Abfrage Werte Richtig?
	    System.out.println(ansi().fg(GREEN).a("Sind alle Werte richtig? Ja/Nein").reset());
	    Scanner scan4 = new Scanner(System.in);
	    String check;
	    check = scan4.nextLine();
	    System.out.println(check);
	    bw.write("Laengen: "+Arrays.toString(werte));
	    bw.newLine();
	    bw.newLine();
	    if (check.equalsIgnoreCase("Ja")) {
	      clear();
	    } 
	    else{
	      clear();
	      System.out.println("Starten sie das Programm neu um die Werte erneut einzugeben");
	      System.exit(0);
	    }
	    
	    
	    //Durchschnitte Berechnen Werte
	    double Summe = 0;
	    
	    double DurschnittWerte;
	    for(int i=0; i < werte.length; i++){
	      Summe = Summe + werte[i];
	    }
	    DurschnittWerte = (double)Summe/werte.length; 
	    System.out.println(ansi().fg(GREEN).a("Durchschnitt Werte: " + DurschnittWerte).reset());
	    System.out.println();
	    bw.write("Durchschnitt Werte: " + DurschnittWerte);
	    bw.newLine();
	    
	    double na = anzahl - 1;
	    double n = (1/na);
	   //System.out.println(n);
	    
	    //Standartabweichung Berechnen
	    double fl;
	    double Standartabweichung1 = 0;
	    for (int i = 0; i <= werte.length - 1 ; i++) {
	      fl = werte[i];
	      fl = fl - DurschnittWerte;
	      fl = fl * fl;
	      Standartabweichung1 = Standartabweichung1 + fl;
	    }
	    double Standartabweichung2 = n * Standartabweichung1;
	    double StandartabweichungFinal= Math.sqrt(Standartabweichung2);
	    System.out.println(ansi().fg(GREEN).a("Standardabweichung: " + StandartabweichungFinal).reset());
	    System.out.println();
	    System.out.println();
	    System.out.println(ansi().fg(RED).a("Ihre Ergebnisse befinden sich jetzt in der Datei \"ausgabe.txt\"").reset());
	    System.out.println(ansi().fg(RED).a("im Verzeichnis wo sich das Programm befindet.").reset());

	    bw.write("Standardabweichung: " + StandartabweichungFinal);
	    bw.newLine();
	    bw.close();
	    AnsiConsole.systemUninstall();
	  }
	    
	  public static void clear(){
	    for (int l = 0; l < 50; ++l) {
	      System.out.println();
	    }
	  }
	}
